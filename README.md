# awesome-scratchpad

A <a href="https://i3wm.org/docs/userguide.html#_scratchpad">scratchpad</a>
like the one from <a href="https://i3wm.org/">i3</a>
for awesome-wm.
# usage

![Usage illustration](img/recordingOfScratchpadUsage.gif)

## install

```bash
cd ~/.config/awesome/
git clone https://gitlab.com/pxxx/awesome-scratchpad
```

## in your awesome wm config file

```lua
local scratchpad = require "awesome-scratchpad"
```
## code snippet examples

```lua
local scratchpad = require "awesome-scratchpad"
```

```lua
-- set custom size (this is optional)
scratchpad:set_size(0.7, 0.7)
```

```lua
scratchpad:show() -- show scratchpad
scratchpad:hide() -- hide scratchpad
```

```lua
scratchpad:add_client(c) -- add client to scratchpad (this will make the client disappear from the screen)
-- (c has to be a client (clients are basiclly windows in awesome))
```

```lua
-- cycle through clients/windows in scratchpad
scratchpad:cycle()
```
## provided functions for usage
| name                             | description                                                                                                                            |
|----------------------------------|----------------------------------------------------------------------------------------------------------------------------------------|
| ```is_client_on_scratchpad(c)``` | Check if client is on scratchpad.                                                                                                      |
| ```add_client(c)```              | Add client to scratchpad.                                                                                                              |
| ```remove_client(c)```           | Remove client from scratchpad.                                                                                                         |
| ```add_remove_client(c)```       | Remove client from scratchpad if it is on it else add client.                                                                          |
| ```hide()```                     | Hide scratchpad.                                                                                                                       |
| ```show()```                     | Show scratchpad.                                                                                                                       |
| ```next()```                     | Show next client of scratchpad.                                                                                                        |
| ```previous()```                 | Show previous client of scratchpad.                                                                                                    |
| ```cycle()```                    | If focused client is on scratchpad hide scratchpad and set index to the index of the next client else show scratchpad.                 |
| ```set_size(width, height)```    | Set size of scratchpad window proportional to screen size. values should be betreen 0 and 1. (default size: width = 0.9, height = 0.9) |

# known issues

## no refresh of clients on tag when adding client to scratchpad

When adding a client to the scratchpad it is therefore removed from its tag.
When doing so the sizes of the other clients on the tag are currently not refreshed immediately,
mining that they keep there sizes although the tag has a client less and should therefore resize the
remaining once in order to distribute the freed space on the screen between the remaining clients.
As soon as you do any action such as switching the tag, showing the scratchpad or resizing clients
it causes awesome to resize the remaining clients properly.

# copyright
You can use this code for every purpose. To mention the source is not required, but appreciated.
