--[[ Description: it is a stack of windows that you can call on any tag an cycle trough
                  similar to the scratchpad of i3]]

-- TODO: update layout when a window is added to the scratchpad and therefore is removed form the screen/tag
local client = client
local awful = require("awful")

local clients_of_scratchpad = {}
local properties_of_clients = {}
local current_index = 1
local scratchpad_window_size_proportional_to_screen_size = { width = 0.9, height = 0.9 }

local function remove_from_table(c, t, compare)
  local table = {}
  for _, e in pairs(t) do
    if not compare(c, e) then
      table[#table + 1] = e
    end
  end
  return table
end

local function remove_client_from_scratchpad_at(index)
  if not clients_of_scratchpad[index] then return end
  for i = index, #clients_of_scratchpad - 1 do
    clients_of_scratchpad[i] = clients_of_scratchpad[i + 1]
  end
  clients_of_scratchpad[#clients_of_scratchpad] = nil
end

local function is_client_alive(c)
  if not c then return false end
  local success, _ = pcall(function() return c.pid end)
  return success
end

local function get_current_living_client()
  local c = clients_of_scratchpad[current_index]
  while not is_client_alive(c) or not c do
    remove_client_from_scratchpad_at(current_index)
    if current_index > #clients_of_scratchpad then
      current_index = math.max(#clients_of_scratchpad, 1)
    end
    c = clients_of_scratchpad[current_index]
    if not c then return nil end
  end
  return c
end

local function are_clients_the_same(a, b)
  local out, state = pcall(
    (function()
      return a.pid == b.pid and a.startup_id == b.startup_id
    end)
  )

  return out and state
end

local function is_client_in_scratchpad(c)
  for _, e in pairs(clients_of_scratchpad) do
    if are_clients_the_same(c, e) then
      return true
    end
  end
  return false
end

local function remove_client_from_tag(c, t)
  t:clients(remove_from_table(c, t:clients(), are_clients_the_same))
end

local function get_properties(c)
  local properties = {
    ["fullscreen"] = c.fullscreen,
    ["maximized"] = c.maximized,
    ["maximized_horizontal"] = c.maximized_horizontal,
    ["maximized_vertical"] = c.maximized_vertical,
    ["ontop"] = c.ontop,
    ["floating"] = c.floating,
    ["below"] = c.below,
    ["sticky"] = c.sticky,
    ["is_fixed"] = c.is_fixed,
    ["border_color"] = c.border_color,
  }
  if c.floating then
    properties.geometry = c:geometry()
  end
  return properties
end

local function get_scratchpad_size()
  local workarea = awful.screen.focused().workarea
  return workarea.width * scratchpad_window_size_proportional_to_screen_size.width,
      workarea.height * scratchpad_window_size_proportional_to_screen_size.height
end

local function center_client(c)
  local workarea = awful.screen.focused().workarea
  c.x = (workarea.width - c.width) / 2
  c.y = (workarea.height - c.height) / 2
end

local function set_properties_of_client(c)
  c.fullscreen           = false
  c.maximized            = false
  c.maximized_horizontal = false
  c.maximized_vertical   = false
  c.floating             = true
  c.ontop                = true
  c.is_fixed             = false
  c.width, c.height      = get_scratchpad_size()
  center_client(c)
end

local function set_next_index()
  current_index = current_index + 1
  if current_index > #clients_of_scratchpad then
    current_index = 1
  end
end

local function set_previous_index()
  current_index = current_index - 1
  if current_index < 1 then
    current_index = #clients_of_scratchpad
  end
end

local function restore_properties(c, properties)
  for p, v in pairs(properties) do
    if p ~= "geometry" then
      c[p] = v
    end
  end

  if properties.geometry then
    c:geometry(properties.geometry)
  end
end

return {
  is_client_on_scratchpad = function(c)
    if not c then return end
    for _, e in pairs(clients_of_scratchpad) do
      if e then
        if are_clients_the_same(e, c) then
          return true
        end
      end
    end
    return false
  end,
  add_client = function(self, c)
    if not c then return end
    if self.is_client_on_scratchpad(c) then return end
    clients_of_scratchpad[#clients_of_scratchpad + 1] = c
    properties_of_clients[c] = get_properties(c)
    remove_client_from_tag(c, awful.screen.focused().selected_tag)
  end,
  remove_client = function(c)
    local properties = properties_of_clients[c]
    properties_of_clients[c] = nil
    clients_of_scratchpad = remove_from_table(c, clients_of_scratchpad, are_clients_the_same)
    restore_properties(c, properties)
  end,
  add_remove_client = function(self, c)
    if not c then return end
    if self.is_client_on_scratchpad(c) then
      self.remove_client(c)
    else
      self:add_client(c)
    end
  end,
  hide = function()
    local c = clients_of_scratchpad[current_index]
    if not c then return end
    c.sticky = false
    remove_client_from_tag(c, awful.screen.focused().selected_tag)
  end,
  show = function()
    local c = get_current_living_client()
    if c == nil then return end
    set_properties_of_client(c)
    local current_tag = awful.screen.focused().selected_tag
    c:move_to_tag(current_tag)
    c:raise()
    client.focus = c
    c.sticky = true
  end,
  next = function(self)
    self.hide()
    set_next_index()
    self.show()
  end,
  previous = function(self)
    self.hide()
    set_previous_index()
    self.show()
  end,
  cycle = function(self)
    local c = client.focus
    if not c or not is_client_in_scratchpad(c) then
      self.show()
    else
      self.hide()
      set_next_index()
    end
  end,
  set_size = function(width, height)
    scratchpad_window_size_proportional_to_screen_size.width, scratchpad_window_size_proportional_to_screen_size.height = width
        , height
  end,
}
